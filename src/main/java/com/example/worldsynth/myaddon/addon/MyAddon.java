package com.example.worldsynth.myaddon.addon;

import com.example.worldsynth.myaddon.module.MyAddonModuleRegister;
import net.worldsynth.WorldSynthDirectoryConfig;
import net.worldsynth.addon.IAddon;
import net.worldsynth.editor.WorldSynthEditorApp;
import net.worldsynth.module.AbstractModuleRegister;
import net.worldsynth.module.addon.IModuleAddon;

import java.util.ArrayList;
import java.util.List;

public class MyAddon implements IAddon, IModuleAddon {

	public static void main(String[] args) {
		WorldSynthEditorApp.startEditor(args, new MyAddon());
	}

	@Override
	public void initAddon(WorldSynthDirectoryConfig directoryConfig) {}

	@Override
	public List<AbstractModuleRegister> getAddonModuleRegisters() {
		ArrayList<AbstractModuleRegister> moduleRegisters = new ArrayList<AbstractModuleRegister>();
		moduleRegisters.add(new MyAddonModuleRegister());
		return moduleRegisters;
	}
}
