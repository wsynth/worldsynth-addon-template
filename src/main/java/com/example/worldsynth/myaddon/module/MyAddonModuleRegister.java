package com.example.worldsynth.myaddon.module;

import com.example.worldsynth.myaddon.module.heightmap.ModuleHeightmapMyFunction;
import net.worldsynth.module.AbstractModuleRegister;
import net.worldsynth.module.ClassNotModuleExeption;

public class MyAddonModuleRegister extends AbstractModuleRegister {
	
	public MyAddonModuleRegister() {
		super();
		
		try {
			registerModule(ModuleHeightmapMyFunction.class, "\\My example addoon");
			
		} catch (ClassNotModuleExeption e) {
			throw new RuntimeException(e);
		}
	}
}
