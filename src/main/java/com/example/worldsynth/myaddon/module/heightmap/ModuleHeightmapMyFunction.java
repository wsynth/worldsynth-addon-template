package com.example.worldsynth.myaddon.module.heightmap;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.module.*;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleParameter;

import java.util.HashMap;
import java.util.Map;

public class ModuleHeightmapMyFunction extends AbstractModule {
	
	private DoubleParameter aParam = new DoubleParameter(
			"aparam", "A parameter",
			"Decription of the parameter",
			10.0, 0.0, Double.POSITIVE_INFINITY, 0, 100);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				aParam
		};
		return p;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("input", new ModuleInputRequest(getInput("Input"), outputRequest.data));
		
		return inputRequests;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
//		double x = requestData.extent.getX();
//		double z = requestData.extent.getZ();
//		double res = requestData.resolution;
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		float normalizedHeight = getSynthParameters().getNormalizedHeightmapHeight();
		
		double aParam = this.aParam.getValue();
		
		//Check if inputs are available
		if (inputs.get("input") == null) {
			//If there is not enough input just return null
			return null;
		}
				
		float[][] heightmap = ((DatatypeHeightmap) inputs.get("input")).getHeightmap();
		
		//----------BUILD----------//
		
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				heightmap[u][v] = myFunction(heightmap[u][v], normalizedHeight, aParam);
			}
		}
		
		requestData.setHeightmap(heightmap);
		
		return requestData;
	}
	
	public float myFunction(float h, float normalizedHeight, double parameter) {
		h /= normalizedHeight;
		
		h = h * h;
		
		return h * normalizedHeight;
	}

	@Override
	public String getModuleName() {
		return "My example function";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Input")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
}
